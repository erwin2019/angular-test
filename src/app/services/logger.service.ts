import { Inject, Injectable, InjectionToken } from '@angular/core';
export interface ILoggerConfig {
    color: string
};
export const LOGGER_CONFIG = new InjectionToken<ILoggerConfig>('LoggerConfig');
export const LOGGER_DI_CONFIG = {color: 'yellow'};


export class LoggerExtra {
    log(text) {
        console.log(`thats my extra logger`);
    }
}

export function myFactory() {
    return new LoggerExtra();
}

@Injectable({
    providedIn: 'root',
    useValue: LOGGER_CONFIG
})
export class LoggerService {

    public id = Math.random();
    constructor(@Inject(LOGGER_CONFIG) public config: ILoggerConfig) {
        console.log("My instance id:", this.id);
    }

    log(text) {
        console.log(`%c text: ${text}`, `color: ${'red'}`);
    }
}
