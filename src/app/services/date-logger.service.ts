import { Injectable } from '@angular/core';
import { LoggerService } from './logger.service';

export class DateLoggerService extends LoggerService {

    log(text) {
        super.log(new Date().toISOString() + text);
    }
}
