import { AfterContentChecked, AfterContentInit, AfterViewChecked, AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, DoCheck, NgZone, OnChanges, OnInit, ViewChild } from '@angular/core';
import { ChildTestComponent } from './components/child-test/child-test.component';
import { DateLoggerService } from './services/date-logger.service';
import { LoggerService, LOGGER_CONFIG } from './services/logger.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{provide: LoggerService, useClass: DateLoggerService}, {provide: LOGGER_CONFIG, useValue: {color: 'blue'}}]
})
export class AppComponent implements OnChanges, OnInit, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked {

    @ViewChild(ChildTestComponent) childTest: ChildTestComponent;
  title = 'testing-app';

  counter = 0;

  testConfig = {
      color: 'orange',
      id: 5
  }

  constructor(private zone: NgZone, private ref: ChangeDetectorRef, private loggerSvc: LoggerService){
    console.log("id App", this.loggerSvc.id);
  }

  ngOnChanges(): void {
    console.log("AppComponent", "onchanges");
  }

  ngOnInit(): void {
    console.log("AppComponent", "oninit");


    //this.zone.runOutsideAngular(() => {
      //this.ref.detach();
      /*setTimeout(() => {
        console.log("Timeout");

      }, 1500);

      setInterval(() => {
        console.log("Interval")
        this.counter++;
        if(this.counter === 5) {

          //this.ref.reattach();
        }
      }, 2000)*/
   // })
  }

  ngDoCheck(): void {
    console.log("AppComponent", "docheck");
  }

  ngAfterContentInit(): void {
    console.log("AppComponent", "aftercontentinit");
  }

  ngAfterContentChecked(): void {
    console.log("AppComponent", "aftercontentchecked");
  }

  ngAfterViewInit(): void {
    console.log("AppComponent", "afterviewinit");

  }

  ngAfterViewChecked(): void {
    console.log("AppComponent", "afterviewchecked");
  }

  logIt(text) {
    this.loggerSvc.log(text);
  }

  changeConf() {
      //this.testConfig = {...this.testConfig, color: 'purple'}
      this.testConfig.id = Math.random()*1000;
      //this.childTest.update()


  }
}
