import { Input } from '@angular/core';
import {
    AfterContentChecked,
    AfterContentInit,
    AfterViewChecked,
    AfterViewInit,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    DoCheck,
    NgZone,
    OnChanges,
    OnInit,
} from '@angular/core';
import { DateLoggerService } from 'src/app/services/date-logger.service';
import { LoggerService, LOGGER_CONFIG } from 'src/app/services/logger.service';

@Component({
    selector: 'app-child-test',
    templateUrl: './child-test.component.html',
    styleUrls: ['./child-test.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        { provide: LoggerService, useClass: DateLoggerService },
        { provide: LOGGER_CONFIG, useValue: { color: 'red' } },
    ],
})
export class ChildTestComponent
    implements
        OnChanges,
        OnInit,
        DoCheck,
        AfterContentInit,
        AfterContentChecked,
        AfterViewInit,
        AfterViewChecked {

    @Input('testInput') testInput;
    @Input('onPushTest') onPushBlumBla;

    counter2 = 0;

    constructor(
        private zone: NgZone,
        private ref: ChangeDetectorRef,
        private loggerSvc: LoggerService
    ) {
        console.log('id Child', this.loggerSvc.id);
    }

    ngOnChanges(): void {
        console.log('ChildTestComponent', 'onchanges');
        //console.log("onpush: ", this.onPushBlumBla);

    }

    ngOnInit(): void {
        console.log('ChildTestComponent', 'oninit');
    }

    ngDoCheck(): void {
        console.log('ChildTestComponent', 'docheck');
    }

    ngAfterContentInit(): void {
        console.log('ChildTestComponent', 'aftercontentinit');
    }

    ngAfterContentChecked(): void {
        console.log('ChildTestComponent', 'aftercontentchecked');
    }

    ngAfterViewInit(): void {
        console.log('ChildTestComponent', 'afterviewinit');
    }

    ngAfterViewChecked(): void {
        console.log('ChildTestComponent', 'afterviewchecked');
    }

    logChild(text) {
        this.loggerSvc.log(text);
    }

    update() {
        this.ref.detectChanges();
    }
}
