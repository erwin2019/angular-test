import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChildTestComponent } from './components/child-test/child-test.component';
import { LoggerService, LOGGER_CONFIG, LOGGER_DI_CONFIG } from './services/logger.service';

@NgModule({
  declarations: [
    AppComponent,
    ChildTestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    {provide: LoggerService, useClass: LoggerService}, {provide: LOGGER_CONFIG, useValue: {color: 'purple'}}
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
